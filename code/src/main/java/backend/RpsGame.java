//Knadry Soufiane
package backend;

import java.util.Random;

public class RpsGame {
    private int wins;
    private int loses;
    private int ties;
    private Random rand;

    public RpsGame() {
        this.wins = 0;
        this.loses = 0;
        this.ties = 0;
        this.rand = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLoses() {
        return this.loses;
    }

    public String playRound(String choice) {
        int random = rand.nextInt(3);
        String computerChoice = "";
        if (random == 0) {
            computerChoice = "rock";
        } else if (random == 1) {
            computerChoice = "paper";
        } else if (random == 2) {
            computerChoice = "scissors";
        }
    
        if (choice.equals(computerChoice)) {
            ties++;
            return "Computer plays " + computerChoice + " and it's a tie";
        } else if ((choice.equals("rock") && computerChoice.equals("scissors")) ||
                   (choice.equals("paper") && computerChoice.equals("rock")) ||
                   (choice.equals("scissors") && computerChoice.equals("paper"))) {
            wins++;
            return "Computer plays " + computerChoice + " and you won!";
        } else {
            loses++;
            return "Computer plays " + computerChoice + " and the computer won";
        }
    }
    
}
