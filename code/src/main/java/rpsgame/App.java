//Knadry Soufiane
package rpsgame;

import backend.RpsGame;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        RpsGame game = new RpsGame();

        boolean playAgain = true;

        while (playAgain) {
            System.out.print("Enter your choice (rock, paper, or scissors), or 'quit' to exit: ");
            String userChoice = scanner.nextLine().toLowerCase();
        
            if (userChoice.equals("quit")) {
                playAgain = false;
                System.out.println("Thanks for playing!");
            } else if (userChoice.equals("rock") || userChoice.equals("paper") || userChoice.equals("scissors")) {
                String roundResult = game.playRound(userChoice);
                System.out.println(roundResult);
                System.out.println("Wins: " + game.getWins() + ", Losses: " + game.getLoses() + ", Ties: " + game.getTies());
            } else {
                System.out.println("Invalid choice. Please enter rock, paper, scissors, or quit.");
            }
        }
        
        scanner.close();
    }
}
